﻿using UnityEngine;
using System.Collections.Generic;

public static class Cache {
	
	static Dictionary<System.Type, Object> findCache = new Dictionary<System.Type, Object>();
	static Dictionary<System.Type, Object[]> findAllCache = new Dictionary<System.Type, Object[]>();
	static Dictionary<string, Object> loadCache = new Dictionary<string, Object>();
	static Dictionary<string, Object[]> loadAllCache = new Dictionary<string, Object[]>();
	
	public static T Find<T>() where T : Object {
		Object result = null;
		if (!findCache.TryGetValue (typeof(T), out result)) {
			result = GameObject.FindObjectOfType<T>();
			if (result != null) {
				findCache[typeof(T)] = result;
			}
		}
		return result as T;
	}
	
	public static T[] FindAll<T>() where T : Object {
		Object[] result = null;
		if (!findAllCache.TryGetValue (typeof(T), out result)) {
			result = GameObject.FindObjectsOfType<T> ();
			if (result != null && result.Length > 0) {
				findAllCache [typeof(T)] = result;
			}
		}
		return result as T[];
	}
	
	public static void ResetFind() {
		findCache.Clear ();
		findAllCache.Clear ();
	}
	
	public static T Load<T>(string path) where T : Object {
		Object result = null;
		if (!loadCache.TryGetValue (path, out result)) {
			result = Resources.Load<T>(path);
			if (result != null) {
				loadCache[path] = result;
			}
		}
		return result as T;
	}
	
	public static T[] LoadAll<T>(string path) where T : Object {
		Object[] result = null;
		if (!loadAllCache.TryGetValue (path, out result)) {
			result = Resources.LoadAll<T>(path);
			if (result != null && result.Length > 0) {
				loadAllCache[path] = result;
			}
		}
		return result as T[];
	}
}
