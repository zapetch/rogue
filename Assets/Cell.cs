﻿using UnityEngine;
using System.Collections.Generic;

public class Cell : MonoBehaviour {

    public Index index;
    public Transform floor;
    public List<Transform> objects;
    public float vision = 0f;

    SpriteRenderer rend;

    void Start() {
        UpdateVision();
    }

    public void UpdateVision() {
        var color = rend.color;
        color.a = 1f - vision;
        rend.color = color;
    }

    public static Cell Create(Index index, Transform floorPrefab) {
        var cell = Instantiate(Cache.Load<Cell>("Cell"));
        cell.rend = cell.GetComponent<SpriteRenderer>();
        cell.name = cell.name + " " + index.ToString();
        cell.transform.position = new Vector3(index.x, index.y);
        cell.index = index;
        cell.floor = Instantiate(floorPrefab);
        cell.floor.transform.parent = cell.transform;
        cell.floor.transform.localPosition = Vector3.zero;
        cell.objects = new List<Transform>();
        return cell;
    }

    public void Add(Transform obj, bool updatePosition = false) {
        obj.transform.parent = transform;
        objects.Add(obj);
        if (updatePosition) {
            obj.transform.localPosition = Vector3.zero;
        }
        if (obj.tag == "solid") {
            Map.instance.solids[index.x, index.y] = -1;
        }
    }

    public void Remove(Transform obj) {
        objects.Remove(obj);
        if (obj.tag == "solid") {
            foreach (var o in objects) {
                if (o.tag == "solid")
                    return;
            }
            Map.instance.solids[index.x, index.y] = 0;
        }
    }

}