﻿using UnityEngine;
using System.Collections.Generic;


public class Creature : MonoBehaviour {

    public static List<Creature> all = new List<Creature>();
    public static int busyCount = 0;

    public List<Part> parts;
    public float animationSpeed = 8f;

    public List<string> canNotPass;

    public int strenght;
    public int agility;
    public int intellect;

    public int maxHealth { get { return 5 * strenght; } }
    public int vision { get { return intellect / 5; } }
    public int maxMoves { get { return agility / 10; } }

    [HideInInspector]
    public int health;
    [HideInInspector]
    public int moves;

    bool isBusy = false;
    List<Index> targets;

    Map map;
    Color color;

    void Start() {
        targets = new List<Index>();
        all.Add(this);
        map = Map.instance;
        if (parts.Count == 0) {
            RandomCreature();
        }
        UpdateParts();
    }

    void RandomCreature() {
        var heads = Cache.LoadAll<Part>("Creatures/Head");
        AddRandomPart(heads);
        var hands = Cache.LoadAll<Part>("Creatures/Hands");
        AddRandomPart(hands);
        var bodies = Cache.LoadAll<Part>("Creatures/Body");
        AddRandomPart(bodies);
        var legs = Cache.LoadAll<Part>("Creatures/Legs");
        AddRandomPart(legs);
    }

    void AddRandomPart(Part[] partPrefabs) {
        var part = Instantiate(partPrefabs[Random.Range(0, partPrefabs.Length)]);
        part.attribute = Random.Range(part.attribute, 2 * part.attribute);
        part.transform.parent = transform;
        part.transform.localPosition = Vector3.zero;
        parts.Add(part);
    }

    void UpdateParts() {
        strenght = 0;
        agility = 0;
        intellect = 0;
        foreach (var p in parts) {
            switch (p.type) {
                case AttributeType.Strenght: strenght += p.attribute; break;
                case AttributeType.Agility: agility += p.attribute; break;
                case AttributeType.Intellect: intellect += p.attribute; break;
                case AttributeType.All:
                    strenght += p.attribute;
                    agility += p.attribute;
                    intellect += p.attribute;
                    break;
            }
        }
        float avg = (strenght + agility + intellect) / 2f;
        color = new Color(strenght / avg, agility / avg, intellect / avg);
        foreach (var p in parts) {
            p.GetComponent<SpriteRenderer>().color = color;
        }
    }

    void Update() {
        if (!isBusy)
            return;

        if (targets.Count > 0) {
            var index = new Index(transform.position);
            var parentIndex = new Index(transform.parent.position);
            if (index.x != parentIndex.x || index.y != parentIndex.y) {
                map.cells[parentIndex.x, parentIndex.y].Remove(transform);
                map.cells[index.x, index.y].Add(transform);
            }
            var targetPosition = new Vector3(targets[0].x, targets[0].y);
            var diff = targetPosition - transform.position;
            if (diff.magnitude < animationSpeed * Time.deltaTime) {
                transform.position = targetPosition;
                targets.RemoveAt(0);
                StepDone();
            } else { 
                transform.Translate(diff.normalized * animationSpeed * Time.deltaTime);
            }
        }
    }

    public void UpdateTurn() {
        moves = maxMoves;
        busyCount++;
        if (targets.Count > 0) {
            isBusy = true;
        }
        SendMessage("BeforeTurn", SendMessageOptions.DontRequireReceiver);
    }

    public void StepDone() {
        moves--;
        if (moves == 0) {
            busyCount--;
            isBusy = false;
        }
        SendMessage("AfterStep", SendMessageOptions.DontRequireReceiver);
    }

    void Wear(Part wearable) {
        for (int i = 0; i < parts.Count; i++) {
            if (parts[i].layer == wearable.layer) {
                parts.RemoveAt(i);
                break;
            }
        }

        var wear = Instantiate(wearable);
        wear.transform.parent = transform;
        wear.transform.localPosition = Vector3.zero;
    }

    public void Move(Cell cell) {
        if (targets.Count > 0) {
            while (targets.Count > 1) {
                targets.RemoveAt(targets.Count - 1);
            }
            return;
        }

        var index = new Index(transform.position);
        var diff = new Index(cell.index.x - index.x, cell.index.y - index.y);
        if (diff.Lenght() > vision)
            return;

        var path = Matrix.Copy(map.solids, index, vision);
        var center = new Index(vision, vision);
        path[center.x, center.y] = 1;
        var target = new Index(center.x + diff.x, center.y + diff.y);
        var positions = new List<Index>() { center };
        targets = Matrix.WaveFind(path, positions, vision, target);
        for (int i = 0; i < targets.Count; i++) {
            var t = new Index(targets[i].x + index.x - center.x, targets[i].y + index.y - center.y);
            targets[i] = t;
        }
        isBusy = true;
    }
}
