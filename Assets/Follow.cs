﻿using UnityEngine;
using System.Collections;

public class Follow : MonoBehaviour {

    public Transform target;
    public float speed;

    void Start() {
        Invoke("UpdatePosition", Time.deltaTime);
    }

    void UpdatePosition() {
        var diff = target.position - transform.position;
        diff.z = 0;
        transform.position += diff;
    }

    void Update () {
        var diff = target.position - transform.position;
        diff.z = 0;
        if (diff.magnitude < speed * Time.deltaTime) {
            transform.position += diff;
        } else {
            transform.position = Vector3.MoveTowards(transform.position, transform.position + diff, (1 + diff.magnitude) * speed * Time.deltaTime);
        }
    }
}
