﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;


public class Map : MonoBehaviour {

    public static Map instance;

    public string terrain;
    public Index size;
    public Index minRoom;
    public Index maxRoom;

    public Text turnText;

    public int turn = 0;
    public Cell[,] cells;

    public int[,] solids;
    public int[,] liquids;
    public int[,] voids;
 
    void Awake() {
        if (instance != null) {
            Destroy(gameObject);
            return;
        }
        instance = this;
        cells = new Cell[size.x, size.y];
        var box = gameObject.AddComponent<BoxCollider2D>();
        box.size = new Vector2(size.x, size.y);
        box.offset = new Vector2((size.x - 1) / 2f, (size.y - 1) / 2f);
    }

    void Start() {
        var floorsPrefabs = Cache.LoadAll<Transform>("Terrains/" + terrain + "/Floor");
        for (int x = 0; x < size.x; x++) {
            for (int y = 0; y < size.y; y++) {
                var prefab = floorsPrefabs[Random.Range(0, floorsPrefabs.Length)];
                cells[x, y] = Cell.Create(new Index(x, y), prefab);
                cells[x, y].transform.parent = transform;
            }
        }

        solids = Matrix.Solid(size);
        Index schemeSize = new Index(size.x / maxRoom.x, size.y / maxRoom.y);
        var scheme = Matrix.Cross(schemeSize);
        var rooms = new List<Index>();
        int roomsCount = 0;
        for (int ri = 0; ri < schemeSize.x; ri++) {
            for (int rj = 0; rj < schemeSize.y; rj++) {
                if (scheme[ri, rj] == 0) {
                    roomsCount++;
                }
            }
        }
        int playerInRoom = Random.Range(0, roomsCount);

        for (int rx = 0; rx < schemeSize.x; rx++) {
            for (int ry = 0; ry < schemeSize.y; ry++) {
                if (scheme[rx, ry] != 0)
                    continue;

                Index rs = new Index(Random.Range(minRoom.x, maxRoom.x), Random.Range(minRoom.y, maxRoom.y));
                var room = Matrix.Any(rs);
                Index offset = new Index(Random.Range(0, maxRoom.x - rs.x), Random.Range(0, maxRoom.y - rs.y));
                for (int i = 0; i < rs.x; i++) {
                    for (int j = 0; j < rs.y; j++) {
                        solids[rx * maxRoom.x + offset.x + i, ry * maxRoom.y + offset.y + j] = room[i, j];
                    }
                }
                var roomCenter = new Index(rx * maxRoom.x + offset.x + rs.x / 2, ry * maxRoom.y + offset.y + rs.y / 2);
                scheme[rx, ry] = rooms.Count;
                rooms.Add(roomCenter);
            }
        }
        for (int rx = 0; rx < schemeSize.x; rx++) {
            for (int ry = 0; ry < schemeSize.y; ry++) {
                if (scheme[rx, ry] < 0)
                    continue;
                if (rx + 1 < schemeSize.x && scheme[rx + 1, ry] >= 0) {
                    Matrix.Connect(solids, rooms[scheme[rx, ry]], rooms[scheme[rx + 1, ry]]);
                }
                if (ry + 1 < schemeSize.y && scheme[rx, ry + 1] >= 0) {
                    Matrix.Connect(solids, rooms[scheme[rx, ry]], rooms[scheme[rx, ry + 1]]);
                }
            }
        }

        var wallsPrefabs = Cache.LoadAll<Transform>("Terrains/" + terrain + "/Wall");
        for (int x = 0; x < size.x; x++) {
            for (int y = 0; y < size.y; y++) {
                if (solids[x, y] == -1) {
                    var prefab = wallsPrefabs[Random.Range(0, wallsPrefabs.Length)];
                    var wall = Instantiate(prefab);
                    wall.GetComponent<SpriteRenderer>().sortingOrder = -y;
                    cells[x, y].Add(wall, true);
                } 
            }
        }

        if (Player.instance != null) {
            cells[rooms[playerInRoom].x, rooms[playerInRoom].y].Add(Player.instance.transform, true);
            Player.instance.UpdateVision();
        }
    }

    void Update() {
        if (Creature.busyCount == 0) {
            NextTurn();
        }
    }

    void NextTurn() {
        foreach (var c in Creature.all) {
            c.UpdateTurn();
        }
        turn++;
        turnText.text = "Turn: " + turn.ToString();
    }

    void OnMouseUpAsButton() {
        var pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        var index = new Index(pos);
        Player.instance.character.Move(cells[index.x, index.y]);
    }
}

