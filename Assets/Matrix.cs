﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public struct Index {
    public int x, y;

    public Index(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Index(Vector2 vec) {
        x = Mathf.RoundToInt(vec.x);
        y = Mathf.RoundToInt(vec.y);
    }

    public int Lenght() {
        return Mathf.Abs(x) + Mathf.Abs(y);
    }

    public int Distance(Index other) {
        return Mathf.Abs(other.x - x) + Mathf.Abs(other.y + y);
    }

    public override string ToString() {
        return string.Format("{0} {1}", x, y);
    }
}


public static class Matrix {

    public delegate int[,] GenerateAlgorithm(Index size);

    public static GenerateAlgorithm[] algorithms = new GenerateAlgorithm[] {
        new GenerateAlgorithm(Solid),
        new GenerateAlgorithm(Empty),
        new GenerateAlgorithm(Cross)
    };

    public static int[,] Any(Index size) {
        var algorithm = algorithms[Random.Range(0, algorithms.Length)];
        return algorithm(size);
    }

    public static int[,] Fill(Index size, int val) {
        var matrix = new int[size.x, size.y];
        for (int x = 0; x < size.x; x++) {
            for (int y = 0; y < size.y; y++) {
                matrix[x, y] = val;
            }
        }
        return matrix;
    }

    public static int[,] Solid(Index size) {
        return Fill(size, -1);
    }

    public static int[,] Empty(Index size) {
        return Fill(size, 0);
    }

    public static int[,] Cross(Index size) {
        int[,] matrix = new int[size.x, size.y];
        for (int x = 0; x < size.x; x++) {
            for (int y = 0; y < size.y; y++) {
                matrix[x, y] = 0;
            }
        }
        Index obstacle = new Index(size.x / 2, size.y / 2);
        for (int x = 0; x < size.x; x++) {
            int ry = Random.Range(0, obstacle.y);
            for (int y = 0; y < ry; y++) {
                matrix[x, y] = -1;
            }
            ry = Random.Range(size.y - obstacle.y, size.y);
            for (int y = size.y - 1; y > ry; y--) {
                matrix[x, y] = -1;
            }
        }
        for (int y = 0; y < size.y; y++) {
            int rx = Random.Range(0, obstacle.x);
            for (int x = 0; x < rx; x++) {
                matrix[x, y] = -1;
            }
            rx = Random.Range(size.x - obstacle.x, size.x);
            for (int x = size.x - 1; x > rx; x--) {
                matrix[x, y] = -1;
            }
        }
        return matrix;
    }

    public static int[,] Copy(int [,] matrix, Index center, int range) {
        Index size = new Index(2 * range + 1, 2 * range + 1);
        var copy = Fill(size, -1);
        for (int dx = -range; dx <= range; dx++) {
            if (center.x + dx < 0 || center.x + dx >= matrix.GetLength(0))
                continue;
            for (int dy = -range; dy <= range; dy++) {
                if (center.y + dy < 0 || center.y + dy >= matrix.GetLength(1))
                    continue;
                copy[range + dx, range + dy] = matrix[center.x + dx, center.y + dy];
            }
        }
        return copy;
    }

    public static int WaveStep(int [,] matrix, List<Index> positions) {
        var newPositions = new List<Index>();
        var range = matrix[positions[0].x, positions[0].y];
        foreach (var p in positions) {
            if (p.x > 0 && matrix[p.x - 1, p.y] == 0) {
                matrix[p.x - 1, p.y] = range + 1;
                newPositions.Add(new Index(p.x - 1, p.y));
            }
            if (p.x + 1 < matrix.GetLength(0) && matrix[p.x + 1, p.y] == 0) {
                matrix[p.x + 1, p.y] = range + 1;
                newPositions.Add(new Index(p.x + 1, p.y));
            }
            if (p.y > 0 && matrix[p.x, p.y - 1] == 0) {
                matrix[p.x, p.y - 1] = range + 1;
                newPositions.Add(new Index(p.x, p.y - 1));
            }
            if (p.y + 1 < matrix.GetLength(1) && matrix[p.x, p.y + 1] == 0) {
                matrix[p.x, p.y + 1] = range + 1;
                newPositions.Add(new Index(p.x, p.y + 1));
            }
        }
        positions.Clear();
        positions.AddRange(newPositions);
        return range + 1;
    }

    public static void WaveFill(int[,] matrix, List<Index> positions, int maxRange) {
        int range = WaveStep(matrix, positions);
        if (range <= maxRange) {
            WaveFill(matrix, positions, maxRange);
        }
    }

    public static List<Index> WaveFind(int [,] matrix, List<Index> positions, int maxRange, Index target) {
        int range = WaveStep(matrix, positions);
        if (matrix[target.x, target.y] > 0) {
            var targets = new List<Index>();
            var p = target;
            while (range > 1) {
                range--;
                var index = new Index(p.x, p.y);
                targets.Insert(0, index);
                if (p.x > 0 && matrix[p.x - 1, p.y] == range) {
                    p.x--;
                } else
                if (p.x < 2 * maxRange && matrix[p.x + 1, p.y] == range) {
                    p.x++;
                } else
                if (p.y > 0 && matrix[p.x, p.y - 1] == range) {
                    p.y--;
                } else
                if (p.y < 2 * maxRange && matrix[p.x, p.y + 1] == range) {
                    p.y++;
                }
            }
            return targets;
        } else if (range <= maxRange) {
            return WaveFind(matrix, positions, maxRange, target);
        }
        return new List<Index>();
    }

    public static void Connect(int [,] matrix, Index start, Index end) {
        Index index = start;
        while (index.x != end.x || index.y != end.y) {
            matrix[index.x, index.y] = 0;
            bool byX = Random.Range(0, Mathf.Abs(end.x - index.x) + Mathf.Abs(end.y - index.y)) < Mathf.Abs(end.x - index.x);
            if (byX) {
                index.x += end.x > index.x ? 1 : -1;
            } else {
                index.y += end.y > index.y ? 1 : -1;
            }
        }
        matrix[index.x, index.y] = 0;
    }

    public static string ToString(int [,] matrix) {
        Index size = new Index(matrix.GetLength(0), matrix.GetLength(1));
        string str = string.Format("{0}, {1}:\n", size.x, size.y);
        for (int i = 0; i < size.x; i++) {
            for (int j = 0; j < size.y; j++) {
                str += matrix[i, j].ToString() + '\t';
            }
            str += '\n';
        }
        return str;
    }

}
