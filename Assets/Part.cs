﻿using UnityEngine;
using System.Collections.Generic;

public enum AttributeType {
    Strenght,
    Agility,
    Intellect,
    All
}


public class Part : MonoBehaviour {

    public bool isOrgan;
    public AttributeType type;
    public int attribute;
    //public List<Spell> spells;

    public string layer {
        get {
            return rend.sortingLayerName;
        }
    }

    public SpriteRenderer rend {
        get {
            if (_rend == null) {
                _rend = GetComponent<SpriteRenderer>();
            }
            return _rend;
        }
    }
    SpriteRenderer _rend = null;

}
