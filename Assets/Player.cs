﻿using UnityEngine;
using System.Collections.Generic;

public class Player : MonoBehaviour {

    public static Player instance;

    public Creature character {
        get {
            if (_character == null) {
                _character = GetComponent<Creature>();
            }
            return _character;
        }
    }
    Creature _character = null;

    void Awake () {
        if (instance != null) {
            Destroy(gameObject);
            return;
        }
        instance = this;
    }

    void BeforeTurn() {
        UpdateVision();
    }

    void AfterStep() {
        UpdateVision();
    }

    public void UpdateVision() {
        var size = Map.instance.size;
        var cells = Map.instance.cells;
        int vision = character.vision;
        int moves = character.moves;
        var index = new Index(transform.position);

        var path = Matrix.Copy(Map.instance.solids, index, vision);
        var center = new Index(vision, vision);
        path[center.x, center.y] = 1;
        var positions = new List<Index>() { center };
        Matrix.WaveFill(path, positions, vision);

        for (int vx = -vision - 1; vx <= vision + 1; vx++) {
            int range = vision + 1 - Mathf.Abs(vx);
            for (int vy = -range; vy <= range; vy++) {
                int x = index.x + vx;
                int y = index.y + vy;
                if (x < 0 || x >= size.x || y < 0 || y >= size.y)
                    continue;
                int dist = Mathf.Abs(vx) + Mathf.Abs(vy);
                var p = new Index(center.x + vx, center.y + vy);
                if (dist > vision || path[p.x, p.y] == 0) {
                    if (cells[x, y].vision > 0f) {
                        cells[x, y].vision = 0.25f;
                    }
                } else
                if (path[p.x, p.y] < 0 || (path[p.x, p.y] == vision + 1 && vision > moves)) {
                    cells[x, y].vision = 0.5f;
                } else
                if (path[p.x, p.y] > moves + 1) {
                    cells[x, y].vision = 0.75f;
                } else {
                    cells[x, y].vision = 1f;
                }
                if (cells[x, y].vision > 0f) {
                    cells[x, y].UpdateVision();
                }
            }
        }
    }
}
